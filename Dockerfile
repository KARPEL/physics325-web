#Grab the latest python image
FROM python:latest

#RUN apt-get update
#RUN apt-get install software-properties-common -y
#RUN add-apt-repository ppa:saiarcot895/myppa
#RUN apt-get update
#RUN apt-get install apt-fast -y

# Install stuff
RUN apt-get update
RUN apt-get install bash git -y
RUN apt-get install --no-install-recommends texlive-latex-recommended texlive-pictures texlive-latex-extra texlive-science -y

# Install python dependencies
ADD ./requirements.txt /tmp/requirements.txt
RUN pip3 install --no-cache-dir -r /tmp/requirements.txt

# Add our code
ADD ./webapp /opt/webapp/
WORKDIR /opt/webapp

# Expose is NOT supported by Heroku
# EXPOSE 5000

# Run the image as a non-root user
#RUN adduser myuser
#USER myuser

# Run the app.  CMD is required to run on Heroku
# $PORT is set by Heroku
CMD gunicorn --bind 0.0.0.0:$PORT wsgi

