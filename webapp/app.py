import subprocess
from pathlib import Path

from flask import Flask, send_from_directory, url_for, render_template

app = Flask(__name__)

MANUALS_REPO_DIR = r'/opt/webapp/physics325'


def git_clone():
    subprocess.run(
        ['git', 'clone', 'https://git.doit.wisc.edu/KARPEL/physics325.git', MANUALS_REPO_DIR],
    )


def git_pull():
    subprocess.run(
        ['git', '-C', MANUALS_REPO_DIR, 'pull']
    )


def git_revision():
    proc = subprocess.run(
        ['git', '-C', MANUALS_REPO_DIR, 'rev-parse', 'HEAD'],
        capture_output = True,
    )

    return proc.stdout.decode('utf-8').strip()


git_clone()

MANUALS_DIR = Path('/opt/webapp/physics325/manuals')


@app.route('/')
def index():
    git_pull()
    rev = git_revision()

    names_and_links = (
        (path.stem, url_for('show_lab_manual_pdf', lab = path.stem))
        for path in MANUALS_DIR.iterdir()
        if path.suffix == '.tex'
    )
    names_and_links = sorted(names_and_links)

    return render_template(
        'index.html',
        names_and_links = names_and_links,
        rev_text = rev,
        rev_link = fr'https://git.doit.wisc.edu/KARPEL/physics325/commit/{rev}',
    )


LATEST_REVISION = {}


def compile_lab_manual(lab: str):
    git_pull()
    rev = git_revision()

    if LATEST_REVISION.get(lab, None) == rev:
        print(f'using cached version of {lab}')
        return

    subprocess.run(
        ['bash', str(MANUALS_DIR / 'compile.sh'), lab]
    )

    LATEST_REVISION[lab] = rev


@app.route('/<lab>')
def show_lab_manual_pdf(lab: str):
    compile_lab_manual(lab)

    return send_from_directory(
        MANUALS_DIR,
        f'{lab}.pdf',
        attachment_filename = f'{lab}.pdf',
        cache_timeout = -1,
    )


if __name__ == '__main__':
    app.run(host = '0.0.0.0')
